# calendar
Simple event management solution.

Openapi: `./openapi.yaml`

## Requirements

- Go 1.20 +
- PostgreSQL 16+
- docker (optional)

## Running Application

Simply go to project root and run:
```bash
docker-compose up -d # optional
GOENV_CALENDAR=$PWD/.env.dev go run .
```

Note*: The configuration is made via global enviroments variables. The user may export what is needed or edit the `.env.dev` file. If no file is passed by default the app tryies to load `./.env.dev` and fails if no file is found. For complete list of all possible variables: inspect the `/prj_root/.env.dev`.

Note**: They are couple of commands to choose from. For complete list of flags and usage every command have its own help.

Note***: The server depends on postgres instance. The user may provide one by editing `.env.dev` file or use the docker-compose file (for local setup for example).

- Run server

```bash
go run . serve
```

- Migrate the DB

To setup the DB you may need to migrate the given DB first:

```bash
go run . migrate up
```

Internally the command wraps the `pressley/goose` so all available commands for goose may be passed here as well (goose [docs](https://github.com/pressly/goose)):

```bash
go run . migrate # status
go run . migrate status # same
go run . migrate create init sql # create new migration template
go run . migrate up # run all migrations
go run . migrate down # revert one migration
go run . migrate reset # reset the DB
```

- Seed the DB

For testing purposes it may be useful to seed testing DB with dummy data. By default it will seed the DB with 10 records; use `-q x` to overwrite the quantity:

```bash
go run . seed -q 10
```

## Running Tests

``` bash
cd /path/to/project
docker-compose up -d # (optional) setup psql
GOENV_CALENDAR=$PWD/.env.test go test ./...
```

Note*: The tests depends on running postgres; it is up to the user to provide an instance. For convenience the docker-compose may be used.


Note**: All tests run in separate schema. For inspecting the data in the DB after a fail test `-ownschema` flag may be useful. It will persist the schema and it will pretty print its name for later expections of the DB state:
``` bash
GOENV_CALENDAR=$PWD/.env.test go test ./... -v -count 1 -run TestFoo/bar/bas -ownschema
```
