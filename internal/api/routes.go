package api

import (
	"net/http"

	"github.com/labstack/echo/v4"

	v1 "gitlab.com/vnj/calendar/internal/api/v1"
)

func RegisterRoutes(
	mux *echo.Echo,

	eventHandler *v1.EventHandler,
) {
	mux.POST("/api/v1/event", eventHandler.Create)
	mux.DELETE("/api/v1/event/:id", eventHandler.Delete)
	mux.GET("/api/v1/events", eventHandler.List)

	mux.GET("/health-check", func(c echo.Context) error {
		return c.String(http.StatusOK, "Orl Korrekt!")
	})
}
