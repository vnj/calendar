package v1

import (
	"context"
	"net/http"
	"time"

	"github.com/labstack/echo/v4"

	"gitlab.com/vnj/calendar/internal/event"
)

type EventService interface {
	Create(ctx context.Context, event event.Event) (event.Event, error)
	Delete(ctx context.Context, id int64) error
	List(ctx context.Context, filter event.Filter) []event.Event
}

type EventHandler struct {
	srv EventService
}

func NewEventHandler(srv EventService) *EventHandler {
	return &EventHandler{
		srv: srv,
	}
}

func (h *EventHandler) Create(c echo.Context) error {
	type request struct {
		Name        string `json:"name"`
		Description string `json:"description"`
		StartDate   string `json:"start_date"`
		EndDate     string `json:"end_date"`
	}

	type response struct {
		ID          uint      `json:"id"`
		Name        string    `json:"name"`
		Description string    `json:"description"`
		StartDate   time.Time `json:"start_date"`
		EndDate     time.Time `json:"end_date"`
		CreatedAt   time.Time `json:"created_at"`
		UpdatedAt   time.Time `json:"updated_at"`
	}

	var req = new(request)

	if err := c.Bind(req); err != nil {
		return c.String(http.StatusBadRequest, "bad request")
	}

	startDate, err := string2time(req.StartDate)
	if err != nil {
		return c.String(http.StatusBadRequest, "wrong start date format")
	}

	endDate, err := string2time(req.EndDate)
	if err != nil {
		return c.String(http.StatusBadRequest, "wrong end date format")
	}

	event, err := h.srv.Create(c.Request().Context(), event.Event{
		Name:        req.Name,
		Description: req.Description,
		StartDate:   startDate,
		EndDate:     endDate,
	})
	if err != nil {
		return c.String(http.StatusBadRequest, "bad request")
	}

	return c.JSON(http.StatusOK, &response{
		ID:          event.ID,
		Name:        event.Name,
		Description: event.Description,
		StartDate:   event.StartDate,
		EndDate:     event.EndDate,
		CreatedAt:   event.CreatedAt,
		UpdatedAt:   event.UpdatedAt,
	})
}

func (h *EventHandler) Delete(c echo.Context) error {
	type request struct {
		ID int64 `param:"id"`
	}

	var req request

	if err := c.Bind(&req); err != nil {
		return c.String(http.StatusBadRequest, "bad request")
	}

	if err := h.srv.Delete(c.Request().Context(), req.ID); err != nil {
		return c.String(http.StatusBadRequest, "bad request")
	}

	return c.NoContent(http.StatusNoContent)
}

func (h *EventHandler) List(c echo.Context) error {
	type request struct {
		Page      *int    `query:"page"`
		PageSize  *int    `query:"page_size"`
		Overlaps  *bool   `query:"overlaps"`
		StartDate *string `query:"start_date"`
		EndDate   *string `query:"end_date"`
	}

	type response struct {
		ID          uint      `json:"id"`
		Name        string    `json:"name"`
		Description string    `json:"description"`
		StartDate   time.Time `json:"start_date"`
		EndDate     time.Time `json:"end_date"`
		CreatedAt   time.Time `json:"created_at"`
		UpdatedAt   time.Time `json:"updated_at"`
	}

	var req request

	if err := c.Bind(&req); err != nil {
		return c.String(http.StatusBadRequest, "bad request")
	}

	f := event.Filter{}

	if req.StartDate != nil && req.EndDate != nil {
		startDate, err := string2time(*req.StartDate)
		if err != nil {
			return c.String(http.StatusBadRequest, "wrong start date format")
		}

		endDate, err := string2time(*req.EndDate)
		if err != nil {
			return c.String(http.StatusBadRequest, "wrong end date format")
		}

		f.StartDate = &startDate
		f.EndDate = &endDate
		f.Overlaps = req.Overlaps != nil && *req.Overlaps
	}

	f.Page = 1
	if req.Page != nil && *req.Page > 1 {
		f.Page = *req.Page
	}

	f.PageSize = 10
	if req.PageSize != nil && *req.PageSize > 1 {
		f.PageSize = *req.PageSize
	}

	events := h.srv.List(c.Request().Context(), f)

	var resp = make([]response, len(events))

	for i, e := range events {
		resp[i] = response{
			ID:          e.ID,
			Name:        e.Name,
			Description: e.Description,
			StartDate:   e.StartDate,
			EndDate:     e.EndDate,
			CreatedAt:   e.CreatedAt,
			UpdatedAt:   e.UpdatedAt,
		}
	}

	return c.JSON(http.StatusOK, resp)
}
