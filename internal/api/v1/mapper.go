package v1

import (
	"time"
)

const layout = "2006-01-02 15:04"

func string2time(str string) (time.Time, error) {
	t, err := time.Parse(layout, str)
	if err != nil {
		return time.Time{}, err
	}

	return t, nil
}
