package cmd

import (
	"os"

	"github.com/spf13/cobra"
	"golang.org/x/exp/slog"

	"gitlab.com/vnj/calendar/internal/config"
	"gitlab.com/vnj/calendar/internal/logx"
	"gitlab.com/vnj/calendar/internal/migrate"
	"gitlab.com/vnj/calendar/internal/psql"
)

func NewMigrateCMD() *cobra.Command {
	return &cobra.Command{
		Use:   "migrate",
		Short: "Migrate the database",

		Run: func(cmd *cobra.Command, args []string) {
			cfg := config.MustRead()

			logger := logx.New(cfg.Logger.Mode).WithGroup("migrate")

			db, err := psql.New(psql.Config{
				Host:   cfg.DB.Host,
				Port:   cfg.DB.Port,
				User:   cfg.DB.Username,
				DBName: cfg.DB.DBName,
				Schema: cfg.DB.Schema,
			})
			if err != nil {
				logger.Error("fail to create psql connection", slog.String("error", err.Error()))
				os.Exit(1)
			}

			sqldb, err := db.GetRawConn()
			if err != nil {
				logger.Error("fail to get raw sql connection", slog.String("error", err.Error()))
				os.Exit(1)
			}

			migrator := migrate.New(sqldb, migrate.Config{Dir: cfg.DB.Migrations})

			var (
				command   = "status"
				arguments = []string{}
			)
			if len(args) == 1 {
				command = args[0]
			} else if len(args) >= 2 {
				command = args[0]
				arguments = args[1:]
			}

			if err := migrator.Run(command, arguments...); err != nil {
				logger.Error("fail to run migrations", slog.String("error", err.Error()))
				panic(err)
			}

			logger.Info("migration done")
		},
	}
}
