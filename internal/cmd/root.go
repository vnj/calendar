package cmd

import "github.com/spf13/cobra"

func NewRootCMD() *cobra.Command {
	var rootCmd = &cobra.Command{
		Use:               "calendar",
		Short:             "Calendar app",
		CompletionOptions: cobra.CompletionOptions{DisableDefaultCmd: true},
		Long:              `Event management tool.`,
	}

	rootCmd.AddCommand(
		NewMigrateCMD(),
		NewSeedCMD(),
		NewServeCMD(),
	)

	return rootCmd
}
