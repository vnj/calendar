package cmd

import (
	"os"

	"github.com/spf13/cobra"
	"golang.org/x/exp/slog"

	"gitlab.com/vnj/calendar/internal/config"
	"gitlab.com/vnj/calendar/internal/logx"
	"gitlab.com/vnj/calendar/internal/psql"
	"gitlab.com/vnj/calendar/internal/seed"
)

var quantity int

func NewSeedCMD() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "seed",
		Short: "Seed running database",

		Run: func(cmd *cobra.Command, args []string) {
			cfg := config.MustRead()

			logger := logx.New(cfg.Logger.Mode).WithGroup("migrate")

			db, err := psql.New(psql.Config{
				Host:   cfg.DB.Host,
				Port:   cfg.DB.Port,
				User:   cfg.DB.Username,
				DBName: cfg.DB.DBName,
				Schema: cfg.DB.Schema,
			})
			if err != nil {
				logger.Error("fail to create psql connection", slog.String("error", err.Error()))
				os.Exit(1)
			}

			if err := seed.New(db.DB, logger).Seed(quantity); err != nil {
				logger.Error("fail to create psql connection", slog.String("error", err.Error()))
				os.Exit(1)
			}

			logger.Info("database seeding done", slog.Int("quantity", quantity))
		},
	}

	cmd.Flags().IntVarP(&quantity, "quantity", "q", 10, "how may events to be created")

	return cmd
}
