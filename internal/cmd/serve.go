package cmd

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/spf13/cobra"
	"golang.org/x/exp/slog"

	"gitlab.com/vnj/calendar/internal/api"
	v1 "gitlab.com/vnj/calendar/internal/api/v1"
	"gitlab.com/vnj/calendar/internal/config"
	"gitlab.com/vnj/calendar/internal/event"
	"gitlab.com/vnj/calendar/internal/logx"
	"gitlab.com/vnj/calendar/internal/psql"
)

func NewServeCMD() *cobra.Command {
	return &cobra.Command{
		Use:   "serve",
		Short: "Run the calendar backend",

		Run: func(cmd *cobra.Command, args []string) {
			cfg := config.MustRead()

			logger := logx.New(cfg.Logger.Mode).WithGroup("migrate")

			db, err := psql.New(psql.Config{
				Host:   cfg.DB.Host,
				Port:   cfg.DB.Port,
				User:   cfg.DB.Username,
				DBName: cfg.DB.DBName,
				Schema: cfg.DB.Schema,
			})
			if err != nil {
				logger.Error("fail to create psql connection", slog.String("error", err.Error()))
				os.Exit(1)
			}

			eventService := event.NewService(db.DB)

			eventHandler := v1.NewEventHandler(eventService)

			server := echo.New()
			server.Use(middleware.Logger())

			api.RegisterRoutes(
				server,
				eventHandler,
			)

			go func() {
				if err := server.Start(fmt.Sprintf(":%d", cfg.Server.Port)); err != nil {
					if err != http.ErrServerClosed {
						logger.Error("server closed unexpecting", slog.String("error", err.Error()))
					}
				}
			}()

			quit := make(chan os.Signal, 1)
			signal.Notify(quit, os.Interrupt)

			<-quit

			ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
			defer cancel()

			if err := server.Shutdown(ctx); err != nil {
				logger.Error("server shuting down fails", slog.String("error", err.Error()))
			}
		},
	}
}
