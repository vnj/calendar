package config

import (
	"fmt"
	"os"
	"path"
	"path/filepath"
	"runtime"

	"github.com/joho/godotenv"
	"github.com/kelseyhightower/envconfig"
)

const prefix = "CALENDAR"

type Config struct {
	Server Server `envconfig:"SERVER"`

	DB DB `envconfig:"DB"`

	Logger Logger `envconfig:"LOGGER"`
}

type Server struct {
	Port int `envconfig:"PORT"`
}

type Logger struct {
	Mode string `envconfig:"MODE"`
}

type DB struct {
	Host       string `envconfig:"HOST"`
	Port       uint   `envconfig:"PORT"`
	Username   string `envconfig:"USERNAME"`
	DBName     string `envconfig:"DATABASE_NAME"`
	Schema     string `envconfig:"SCHEMA"`
	Migrations string `envconfig:"MIGRATIONS_LOCATION"`
}

// Read tries to read the app config.
//
// The GOENV_CALENDAR env var determines the config file path and defaults to ".env.dev"
// (Non-absolute paths are treated as relative to the project's root dir).
func MustRead() Config {
	var file string

	if e := os.Getenv("GOENV_CALENDAR"); e != "" {
		file = e
	} else {
		file = ".env.dev"

		fmt.Fprintf(os.Stdout, "GOENV_CALENDAR is unset, default to %s\n", file)
	}

	cfg, err := read(file)
	if err != nil {
		panic(err)
	}

	return cfg
}

func read(file string) (Config, error) {
	cfg := Config{}

	if err := godotenv.Load(file); err != nil {
		return cfg, fmt.Errorf("failed to read config file: %w", err)
	}

	if err := envconfig.Process(prefix, &cfg); err != nil {
		return cfg, fmt.Errorf("failed to read config from environment variables: %w", err)
	}

	return cfg, nil
}

// RootPath returns base project path.
//
// This is reliable to be called from any directory. This makes sense to be
// used in tests (but not only).
func RootPath() string {
	_, b, _, ok := runtime.Caller(0)
	if !ok {
		return ""
	}

	d := path.Join(path.Dir(path.Dir(b)))

	return filepath.Dir(d)
}
