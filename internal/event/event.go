package event

import (
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

type Event struct {
	gorm.Model

	Name        string
	Description string
	StartDate   time.Time
	EndDate     time.Time
	GroupID     uuid.UUID
}
