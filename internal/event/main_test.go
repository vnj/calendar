package event_test

import (
	"fmt"
	"os"
	"testing"

	"gitlab.com/vnj/calendar/internal/test"
)

func TestMain(m *testing.M) {
	statusCode := m.Run()
	if err := test.ClearTestSchemas(); err != nil {
		fmt.Printf("Clear test schemas: %v", err)
		statusCode = 1
	}
	os.Exit(statusCode)
}
