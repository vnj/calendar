package event

import (
	"context"
	"time"

	"gorm.io/gorm"
)

type Filter struct {
	Page      int
	PageSize  int
	Overlaps  bool
	StartDate *time.Time
	EndDate   *time.Time
}

type service struct {
	db *gorm.DB
}

func NewService(db *gorm.DB) *service {
	return &service{
		db: db,
	}
}

func (s *service) Create(ctx context.Context, event Event) (Event, error) {
	rawq := `
		with tmp (group_id) as (
			select group_id from events
			where start_date <= ? AND end_date > ?
			limit 1
		)
		INSERT INTO events (name, description, start_date, end_date, created_at, updated_at, group_id)
		select  ?, ?, ?, ?, now(), now(), (
			select case
				when tmp is null then gen_random_uuid()
				else tmp.group_id
				end
			from tmp
		) returning events.*
`

	var res Event
	err := s.db.Raw(rawq,
		event.StartDate,
		event.StartDate,

		event.Name,
		event.Description,
		event.StartDate,
		event.EndDate,
	).Scan(&res).Error

	if err != nil {
		return Event{}, err
	}

	return res, nil
}

func (s *service) Delete(ctx context.Context, id int64) error {
	return s.db.Delete(&Event{}, id).Error
}

func (s *service) List(ctx context.Context, filter Filter) []Event {
	var events []Event

	offset := (filter.Page - 1) * filter.PageSize

	db := s.db.Offset(offset).Limit(filter.PageSize)

	if filter.StartDate != nil && filter.EndDate != nil {
		db = db.Where("start_date <= ?", *filter.StartDate).
			Where("end_date > ?", *filter.EndDate)
	}

	db.Find(&events)

	return events
}
