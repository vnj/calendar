package event_test

import (
	"context"
	"fmt"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"golang.org/x/exp/slog"

	"gitlab.com/vnj/calendar/internal/event"
	"gitlab.com/vnj/calendar/internal/seed"
	"gitlab.com/vnj/calendar/internal/test"
)

func TestService(t *testing.T) {
	t.Run("create", testCreate)
	t.Run("delete", testDelete)
	t.Run("list", testList)
}

func testCreate(t *testing.T) {
	tm := time.Date(2021, time.Month(2), 21, 1, 10, 30, 0, time.UTC)

	cases := []struct {
		name         string
		err          string
		target       event.Event
		customEvent  []event.Event
		expectedName string
	}{
		{
			name:         "success",
			target:       newEvent(tm, 1, 2),
			expectedName: "custom event",
		},
		{
			name:         "success/conflict",
			target:       newEvent(tm, 1, 2),
			customEvent:  newCustomEvents(tm, "conflicting event", 5),
			expectedName: "custom event",
		},
		{
			name:        "failure/conflict/target within range",
			target:      newEvent(tm, 1, 2),
			customEvent: newCustomEvents(tm, "custom event", 5),
			err:         "events_name_group_id_key",
		},
		{
			name:        "failure/conflict/target start only range",
			target:      newEvent(tm, 2, 7),
			customEvent: newCustomEvents(tm, "custom event", 5),
			err:         "events_name_group_id_key",
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			//
			// setup
			//
			require := require.New(t)

			ctx := context.Background()
			db := test.NewPsql(t)
			service := event.NewService(db)

			seed.New(db, slog.Default()).Seed(3, tc.customEvent...)

			//
			// run
			//
			res, err := service.Create(ctx, tc.target)

			//
			// assert
			//

			if !test.ErrorContains(err, tc.err) {
				require.Fail(fmt.Sprintf("error found: %v, but require: %v", err, tc.err))
			}

			require.Equal(tc.expectedName, res.Name)
		})
	}
}

func testDelete(t *testing.T) {
	cases := []struct {
		name string
	}{
		{
			name: "success",
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			//
			// setup
			//
			require := require.New(t)

			ctx := context.Background()
			db := test.NewPsql(t)
			service := event.NewService(db)

			seed.New(db, slog.Default()).Seed(10)

			//
			// run
			//
			err := service.Delete(ctx, 3)

			//
			// assert
			//
			require.NoError(err)

			var res event.Event
			require.NoError(db.Unscoped().Where("id = ?", 3).First(&res).Error)
			require.NotNil(res.DeletedAt)
		})
	}
}

func testList(t *testing.T) {
	cases := []struct {
		name            string
		expectedLen     int
		expecterFirstID uint
		filter          event.Filter
	}{
		{
			name:            "all",
			filter:          event.Filter{Page: 1, PageSize: 10},
			expectedLen:     10,
			expecterFirstID: 1,
		},
		{
			name:            "second page",
			filter:          event.Filter{Page: 2, PageSize: 3},
			expectedLen:     3,
			expecterFirstID: 4,
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			//
			// setup
			//
			require := require.New(t)

			ctx := context.Background()
			db := test.NewPsql(t)
			service := event.NewService(db)

			seed.New(db, slog.Default()).Seed(10)

			//
			// run
			//
			res := service.List(ctx, tc.filter)

			//
			// assert
			//
			require.EqualValues(tc.expectedLen, len(res))
			require.EqualValues(tc.expecterFirstID, res[0].ID)
		})
	}
}

func newEvent(t time.Time, s, e int) event.Event {
	return event.Event{
		Name:        "custom event",
		Description: "custom desc",
		StartDate:   t.Add(time.Duration(s) * time.Hour),
		EndDate:     t.Add(time.Duration(e) * time.Hour),
	}
}

func newCustomEvents(t time.Time, name string, e int) []event.Event {
	return []event.Event{
		{
			Name:        name,
			Description: "descr",
			GroupID:     uuid.New(),
			StartDate:   t,
			EndDate:     t.Add(time.Duration(e) * time.Hour),
		},
	}
}
