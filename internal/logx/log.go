package logx

import (
	"os"

	"golang.org/x/exp/slog"
)

type logger struct {
	*slog.Logger
}

func New(mode string) *logger {
	var handler slog.Handler

	switch mode {
	case "json":
		handler = slog.NewJSONHandler(os.Stdout, nil)
	case "text":
		handler = slog.NewTextHandler(os.Stdout, nil)
	default:
		handler = slog.NewTextHandler(os.Stdout, nil)
	}

	log := slog.New(handler)

	return &logger{
		Logger: log,
	}
}
