package migrate

import (
	"database/sql"

	"github.com/pressly/goose"
)

type Config struct {
	Dir string
}

type migrator struct {
	db  *sql.DB
	cfg Config
}

func New(db *sql.DB, cfg Config) *migrator {
	return &migrator{
		db:  db,
		cfg: cfg,
	}
}

func (m *migrator) Run(command string, args ...string) error {
	return goose.Run(command, m.db, m.cfg.Dir, args...)
}
