package psql

import (
	"database/sql"
	"fmt"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type psql struct {
	*gorm.DB
}

func New(cfg Config) (*psql, error) {
	dsn := fmt.Sprintf(
		dsnfmt,
		cfg.Host,
		cfg.User,
		cfg.DBName,
		cfg.Port,
		cfg.Schema,
	)

	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})

	return &psql{
		DB: db,
	}, err
}

func (p *psql) GetRawConn() (*sql.DB, error) {
	return p.DB.DB()
}
