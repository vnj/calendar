package psql

const dsnfmt = "host=%s user=%s password=gorm dbname=%s port=%d search_path=%s sslmode=disable"

type Config struct {
	Host   string
	Port   uint
	User   string
	DBName string
	Schema string
}
