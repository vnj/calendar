package seed

import (
	"time"

	"github.com/google/uuid"
	"github.com/jaswdr/faker"
	"golang.org/x/exp/slog"
	"gorm.io/gorm"

	"gitlab.com/vnj/calendar/internal/event"
)

type seeder struct {
	db     *gorm.DB
	fake   faker.Faker
	logger *slog.Logger
}

func New(db *gorm.DB, logger *slog.Logger) *seeder {
	fake := faker.New()
	return &seeder{
		db:     db,
		fake:   fake,
		logger: logger,
	}
}

// Seed seeds db with fake records.
//
// It will create numRecords of fake records with random data.
//
// if events is passed it will INSERT them as they are. This means
// overall the seeded records should be numRecords+len(events).
func (s *seeder) Seed(numRecords int, events ...event.Event) error {
	// var spinner = "[.....]"

	for i := 0; i < numRecords; i++ {
		// fmt.Println(spinner)
		// if i%(20*numRecords/100) == 0 {
		// 	spinner = strings.Replace(spinner, ".", "=", 1)
		// 	fmt.Println(spinner)
		// }

		sd, ed := s.fakeDates()

		ent := event.Event{
			Name:        s.fake.App().Name(),
			Description: s.fake.Lorem().Text(12),
			StartDate:   sd,
			EndDate:     ed,
			GroupID:     uuid.New(),
		}

		if err := s.db.Create(&ent).Error; err != nil {
			return err
		}
	}

	for _, e := range events {
		e := e

		if err := s.db.Create(&e).Error; err != nil {
			return err
		}
	}

	return nil
}

func (s *seeder) fakeDates() (time.Time, time.Time) {
	max := time.Now().AddDate(1, 0, 0)

	sd := s.fake.Time().Time(max)

	// create end_time between 30 min and 6h (360)
	i := s.fake.IntBetween(30, 360)

	ed := sd.Add(time.Duration(i) * time.Minute)

	return sd, ed
}
