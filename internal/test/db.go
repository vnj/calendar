package test

import (
	"flag"
	"fmt"
	"log"
	"path"
	"sync"
	"testing"
	"time"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"

	"gitlab.com/vnj/calendar/internal/config"
	"gitlab.com/vnj/calendar/internal/migrate"
)

// ownschema is a flag that for instance of NewPsql will create
// new schema and will keep it. It is useful for debugging some tedious
// test for example.
//
// The command will print the schema name to the stdout.
var ownschema = flag.Bool("ownschema", false, "run test(s) within own schema")

var (
	dsn          string
	schemasStore sync.Map
)

// NewPsql will create new postgresql.PostgreSQL instance for testing purposes.
func NewPsql(t *testing.T) *gorm.DB {
	t.Helper()

	cfg := config.MustRead()

	schema := "test_" + fmt.Sprintf("%v", time.Now().UnixNano())

	schemasStore.Store(schema, true)

	dsn = connStr(schema, cfg.DB)

	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		t.Fatalf("fail to create a db connection: %v", err)
	}

	if _, err := db.Raw(fmt.Sprintf("CREATE SCHEMA IF NOT EXISTS %q", schema)).Rows(); err != nil {
		t.Fatal(err)
	}

	sqldb, err := db.DB()
	if err != nil {
		t.Fatal(err)
	}

	dir := path.Join(config.RootPath(), cfg.DB.Migrations)

	if err := migrate.New(sqldb, migrate.Config{Dir: dir}).Run("up"); err != nil {
		t.Fatal(err)
	}

	return db
}

// ClearTestSchemas removes any schemas created by NewDB. The functions is
// intended to be called from TestMain.
//
// Example:
//
//	func TestMain(m *testing.M) {
//		statusCode := m.Run()
//		if err := test.ClearTestSchemas(); err != nil {
//			fmt.Printf("Clear test schemas: %v", err)
//			statusCode = 1
//		}
//		os.Exit(statusCode)
//	}
func ClearTestSchemas() error {
	cfg := config.MustRead()

	db, err := gorm.Open(postgres.Open(connStr("calendar", cfg.DB)), &gorm.Config{})
	if err != nil {
		log.Printf("failed to open a connection: %v", err)
	}

	schemasStore.Range(func(key, _ any) bool {
		if *ownschema {
			// keep existing schemas;
			// drop them manually;

			fmt.Printf("--> schema \033[34m%q\033[0m was created <--", key)

			return true
		}
		if _, err := db.Raw(fmt.Sprintf("DROP SCHEMA %q CASCADE", key)).Rows(); err != nil {
			log.Printf("failed to drop %q: %v. DROP IT MANUALLY!\n", key, err)
		}

		return true
	})

	return nil
}

func connStr(schema string, cfg config.DB) string {
	return fmt.Sprintf(
		"host=%s port=%d user=%s dbname=%s search_path=%s sslmode=disable",
		cfg.Host, cfg.Port, cfg.Username, cfg.DBName, schema,
	)
}
