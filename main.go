package main

import (
	"fmt"
	"os"

	"gitlab.com/vnj/calendar/internal/cmd"
)

func main() {
	if err := cmd.NewRootCMD().Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
