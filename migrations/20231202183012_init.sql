-- +goose Up
-- +goose StatementBegin
CREATE TABLE events (
	id BIGSERIAL PRIMARY KEY,
	name VARCHAR(256),
	description VARCHAR(4096),
	start_date TIMESTAMP WITH TIME ZONE,
	end_date TIMESTAMP WITH TIME ZONE,
	group_id UUID,

	created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
	updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
	deleted_at TIMESTAMP WITH TIME ZONE,

	unique(name, group_id)
);

CREATE INDEX start_date_end_date_mm_idx ON events (start_date, end_date);
-- +goose StatementEnd

-- +goose Down
DROP TABLE events;
